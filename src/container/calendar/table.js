import React, { useEffect, useState, useMemo } from "react";
import { Table, Button } from "antd";
import { data } from "../../constant/data-demo";
import { week } from "../../constant/week";
import moment from "moment";
const iconBlockGray = require("../../acssets/image/block-gray.png");
const iconBlockWhite = require("../../acssets/image/block-white.png");

const TableCalendar = (props) => {
  const { time } = props;
  const [numberDays, setNumberDays] = useState(
    moment(time, "YYYY-MM").daysInMonth()
  );

  useEffect(() => {
    setNumberDays(moment(time, "YYYY-MM").daysInMonth());
  }, [time]);

  const renderColor = (key) => {
    switch (key) {
      case "遅通":
        return {
          background: "#afe305",
          fontWeight: 500,
        };
      case "公":
        return {
          background: "#f5b376",
          color: "red",
          fontWeight: 500,
        };
      case "早":
        return {
          background: "#76f3f5",
          fontWeight: 500,
        };
      case "遅":
        return {
          background: "#f5769c",
          fontWeight: 500,
        };
      case "早通":
        return {
          background: "#f5769c",
          fontWeight: 500,
        };
      default:
        return {
          background: "#76f3f5",
          fontWeight: 500,
        };
    }
  };

  const renderBgColor = (val) => {
    if (val === "公") {
      return {
        backgroundImage: `url(${iconBlockGray})`,
        // backgroundSize: "cover",
        backgroundSize: "70% 70%",
        backgroundColor: "#f5b376",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        fontWeight: 500,
        color: "red",
      };
    } else {
      return {
        backgroundImage: `url(${iconBlockWhite})`,
        // backgroundSize: "cover",
        backgroundSize: "70% 70%",
        backgroundColor: "#76f3f5",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        fontWeight: 500,
      };
    }
  };

  const rederColums = (days) => {
    const col = [
      {
        dataIndex: "name",
        width: 150,
        key: 0,
        fixed: "left",
        render: (value, record, index) => {
          const obj = {
            children: value,
            props: {
              style: { fontWeight: 500 },
            },
          };
          // These two are merged into above cell
          if (index === 3) {
            obj.props.rowSpan = 2;
          }
          if (index === 4) {
            obj.props.rowSpan = 0;
          }
          // Render Btn Action
          if (index === 3) {
            obj.children = (
              <div style={{ display: "flex" }}>
                <Button
                  style={{
                    backgroundColor: "#ebe8e8",
                    color: "black",
                    borderColor: "#a1a1a1",
                    borderRadius: 3,
                    marginRight: "5px",
                  }}
                >
                  Btn 1
                </Button>
                <Button
                  style={{
                    backgroundColor: "#ebe8e8",
                    color: "black",
                    borderColor: "#a1a1a1",
                    borderRadius: 3,
                  }}
                >
                  Btn 2
                </Button>
              </div>
            );
            obj.props.style = { ...obj.props.style, background: "#3db9d3" };
          }
          // Render Title Row > 4
          if (index > 4) {
            obj.children = (
              <div>
                <div>{record.name}</div>
                <div style={{ fontSize: "10px", color: "#365aff" }}>
                  {record.content}
                </div>
              </div>
            );
            obj.props.style = { ...obj.props.style, background: "#ebe8e8" };
          }
          return obj;
        },
      },
    ];
    for (let i = 1; i <= days; i++) {
      col.push({
        dataIndex: i.toString(),
        key: i,
        align: "center",
        render: (value, row, index) => {
          const obj = {
            children: value,
            props: {},
          };
          // <Render Row 0
          if (index === 0) {
            if (value < 0) {
              obj.props.style = {
                background: "red",
                color: "white",
                fontWeight: 500,
              };
              return obj;
            } else if (value > 0) {
              obj.children = `+${value}`;
              obj.props.style = {
                background: "#3db9d3",
                color: "white",
                fontWeight: 500,
              };
              return obj;
            }
            obj.props.style = {
              fontWeight: 500,
            };
            return obj;
          }
          // Render Row 0 >

          // <Render Row 3 + 4
          if (index === 3 || index === 4) {
            let colorVal = "white";
            if (moment(`${time}-${value}`).format("dddd") === "Sunday") {
              colorVal = "red";
            }
            if (moment(`${time}-${value}`).format("dddd") === "Saturday") {
              colorVal = "#1f48ff";
            }
            if (index === 4) {
              obj.children = week[moment(`${time}-${value}`).format("dddd")];
            }
            obj.props.style = {
              background: "#3db9d3",
              color: colorVal,
              fontWeight: 600,
            };
          }
          // Render Row 3 + 4>
          // <(Render Row > 4 && Row < 12)
          if (4 < index && index < 12) {
            obj.props.style = renderColor(value);
          }
          // (Render Row > 4 && Row < 12)>

          if (index > 11) {
            obj.props.style = renderBgColor(value);
          }
          return obj;
        },
      });
    }
    return col;
  };
  return (
    <Table
      columns={rederColums(numberDays)}
      dataSource={data}
      pagination={false}
      bordered
      showHeader={false}
      scroll={{ x: 2000 }}
      size={"small"}
    />
  );
};

export default TableCalendar;
