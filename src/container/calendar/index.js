import React, { useState } from "react";
import styled from "styled-components";
import TableCalendar from "./table";
import { Button, Row, Col } from "antd";
import Moment from "moment";
import {
  MenuOutlined,
  PrinterFilled,
  SyncOutlined,
  UserOutlined,
  LeftOutlined,
  RightOutlined,
  CopyOutlined,
  StopOutlined,
  ExportOutlined,
} from "@ant-design/icons";

const Calendar = () => {
  const timeDefaut = Moment().format("YYYY-MM");
  const [time, setTime] = useState(timeDefaut);
  const [tabsActive, setTabsActive] = useState(1);
  const renderBtnHeader = () => {
    return (
      <BtnContainer>
        <Button
          style={{
            backgroundColor: "#329cb4",
            borderRadius: "5px",
            borderColor: "#329cb4",
            marginRight: "5px",
          }}
          type="primary"
          size="large"
        >
          Action 1
        </Button>
        <Button
          style={{
            backgroundColor: "#329cb4",
            borderRadius: "5px",
            borderColor: "#329cb4",
            marginRight: "5px",
          }}
          type="primary"
          size="large"
          icon={<PrinterFilled style={{ color: "white" }} />}
        >
          Action 2
        </Button>
        <Button
          style={{
            backgroundColor: "#329cb4",
            borderRadius: "5px",
            borderColor: "#329cb4",
            marginRight: "5px",
          }}
          type="primary"
          size="large"
          icon={<SyncOutlined style={{ color: "white" }} />}
        >
          Action 3
        </Button>
        <Button
          style={{
            backgroundColor: "#329cb4",
            borderRadius: "5px",
            borderColor: "#329cb4",
            marginRight: "5px",
          }}
          type="primary"
          size="large"
        >
          Action 4
        </Button>
        <Button
          style={{
            backgroundColor: "#329cb4",
            borderRadius: "5px",
            borderColor: "#329cb4",
            marginRight: "10px",
          }}
          type="primary"
          size="large"
          icon={<UserOutlined style={{ color: "white" }} />}
        >
          Action 5
        </Button>
        <Button
          type="primary"
          shape="circle"
          icon={<MenuOutlined style={{ color: "#329cb4" }} />}
          size="large"
          style={{
            backgroundColor: "white",
            borderColor: "#329cb4",
            marginRight: "15px",
          }}
        />
      </BtnContainer>
    );
  };

  const handleNextMonth = () => {
    setTime(Moment(time).add(1, "M").format("YYYY-MM"));
  };
  const handlePreviousMonth = () => {
    setTime(Moment(time).add(-1, "M").format("YYYY-MM"));
  };

  const renderHeaderTable = () => {
    const styleBtnTabs = {
      borderTopLeftRadius: "10px",
      borderTopRightRadius: "10px",
      borderBottomLeftRadius: "0px",
      borderBottomRightRadius: "0px",
      marginRight: "5px",
      width: "33.33%",
      fontSize: "13px",
      padding: "0px",
      fontWeight: 600,
    };
    return (
      <Row>
        <Col span={8}>
          <div style={{ display: "flex", justifyContent: "flex-start" }}>
            <Button
              style={{
                backgroundColor: "#ebe8e8",
                color: "#ebe8e8",
                borderColor: "#a1a1a1",
                borderRadius: 3,
              }}
              size="small"
              icon={<LeftOutlined style={{ color: "black" }} />}
              onClick={handlePreviousMonth}
            />
            <div
              style={{
                marginLeft: "5px",
                marginRight: "5px",
                backgroundColor: "#ebe8e8",
                fontWeight: 600,
                borderRadius: 3,
              }}
            >
              {time}
            </div>
            <Button
              style={{
                backgroundColor: "#ebe8e8",
                color: "#ebe8e8",
                borderColor: "#a1a1a1",
                borderRadius: 3,
              }}
              size="small"
              icon={<RightOutlined style={{ color: "black" }} />}
              onClick={handleNextMonth}
            />
          </div>
        </Col>
        <Col span={8}>
          <TabsTableContainer>
            <Button
              style={{
                ...styleBtnTabs,
                backgroundColor: tabsActive === 1 ? "#ffebb5" : "#329cb4",
                borderColor: tabsActive === 1 ? "#ffebb5" : "#329cb4",
                color: tabsActive === 1 ? "black" : "white",
              }}
              type="primary"
              size="large"
              onClick={() => setTabsActive(1)}
            >
              人時確認
            </Button>
            <Button
              style={{
                ...styleBtnTabs,
                backgroundColor: tabsActive === 2 ? "#ffebb5" : "#329cb4",
                borderColor: tabsActive === 2 ? "#ffebb5" : "#329cb4",
                color: tabsActive === 2 ? "black" : "white",
              }}
              type="primary"
              size="large"
              onClick={() => setTabsActive(2)}
            >
              勤務パターン確認
            </Button>
            <Button
              style={{
                ...styleBtnTabs,
                backgroundColor: tabsActive === 3 ? "#ffebb5" : "#329cb4",
                borderColor: tabsActive === 3 ? "#ffebb5" : "#329cb4",
                color: tabsActive === 3 ? "black" : "white",
              }}
              type="primary"
              size="large"
              onClick={() => setTabsActive(3)}
            >
              スタッフグループ確認
            </Button>
          </TabsTableContainer>
        </Col>
        <Col span={8}>
          <BtnTableRigtContainer>
            <Button
              style={{
                backgroundColor: "#ebe8e8",
                color: "#ebe8e8",
                borderColor: "#a1a1a1",
                borderRadius: 3,
                marginRight: 5,
              }}
              size="middle"
              icon={<ExportOutlined style={{ color: "black" }} />}
            />
            <Button
              style={{
                backgroundColor: "#ebe8e8",
                color: "#ebe8e8",
                borderColor: "#a1a1a1",
                borderRadius: 3,
                marginRight: 5,
              }}
              size="middle"
              icon={<CopyOutlined style={{ color: "black" }} />}
            />
            <Button
              style={{
                backgroundColor: "#ebe8e8",
                color: "#ebe8e8",
                borderColor: "#a1a1a1",
                borderRadius: 3,
              }}
              size="middle"
              icon={<StopOutlined style={{ color: "black" }} />}
            />
          </BtnTableRigtContainer>
        </Col>
      </Row>
    );
  };

  return (
    <div>
      <Header>
        <TitleHeader>Tab Calendar</TitleHeader>
        {renderBtnHeader()}
      </Header>
      <div
        style={{
          marginRight: "10px",
          marginLeft: "10px",
          marginBottom: "50px",
        }}
      >
        {renderHeaderTable()}
        <TableCalendar time={time} />
      </div>
    </div>
  );
};

const Header = styled.div`
  background-color: #3db9d3;
  margin-bottom: 16px;
  display: flex;
  text-align: center;
  justify-content: space-between;
  padding: 15px;
`;

const TitleHeader = styled.div`
  display: flex;
  color: white;
  font-weight: 600;
  align-items: center;
`;

const BtnContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TabsTableContainer = styled.div`
  display: flex;
  justify-content: "center";
`;
const BtnTableRigtContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export default Calendar;
