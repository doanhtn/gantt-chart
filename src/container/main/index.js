import React from "react";
import GanttChart from "../gantt-chart";
import Costs from "../costs";
import Setting from "../setting";
import Calendar from "../calendar";
import "./index.css";
import styled from "styled-components";
import { Tabs } from "antd";
import {
  SettingFilled,
  CalendarOutlined,
  AccountBookOutlined,
  TableOutlined,
} from "@ant-design/icons";

const { TabPane } = Tabs;

const Main = () => {
  const renderIconTabs = (val) => {
    switch (val) {
      case 1:
        return <AccountBookOutlined style={{ fontSize: "20px" }} />;
        break;
      case 2:
        return <TableOutlined style={{ fontSize: "20px" }} />;
        break;
      case 3:
        return <CalendarOutlined style={{ fontSize: "20px" }} />;
        break;
      case 4:
        return <SettingFilled style={{ fontSize: "20px" }} />;
        break;
      default:
        break;
    }
  };

  const renderHeaderTabs = (title, key) => {
    return (
      <ItemTabs>
        <IconTabs>{renderIconTabs(key)}</IconTabs>
        <TitleTabs>{title}</TitleTabs>
      </ItemTabs>
    );
  };

  return (
    <div className="card-container">
      <Tabs defaultActiveKey="2" type="card">
        <TabPane tab={renderHeaderTabs("予算管理", 1)} key="1">
          <Costs />
        </TabPane>
        <TabPane tab={renderHeaderTabs("タイムテーブル", 2)} key="2">
          <GanttChart />
        </TabPane>
        <TabPane tab={renderHeaderTabs("シフト表", 3)} key="3">
          <Calendar />
        </TabPane>
        <TabPane tab={renderHeaderTabs("基本設定", 4)} key="4">
          <Setting />
        </TabPane>
      </Tabs>
    </div>
  );
};

const ItemTabs = styled.div``;
const IconTabs = styled.div`
  display: flex;
  justify-content: center;
`;
const TitleTabs = styled.div`
  display: flex;
  justify-content: center;
`;

export default Main;
