import React from "react";
import Chart from "./chart";
import styled from "styled-components";

const GanttChart = () => {
  return (
    <div>
      <Header>Tab Chart</Header>
      <Chart />
    </div>
  );
};

const Header = styled.div`
  background-color: #3db9d3;
  // margin-bottom: 16px;
  display: flex;
  text-align: center;
`;
export default GanttChart;
