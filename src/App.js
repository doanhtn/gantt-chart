import React from "react";
import Main from "./container/main";
import "antd/dist/antd.css";

const App = () => {
  return <Main />;
};

export default App;
